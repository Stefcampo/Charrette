# Introduction

Cette notice vous a été remise avec votre Charrette. Elle a été fabriquée, assemblée, réglée et vérifiée par une personne qualifiée du cycle disposant d’une formation et de matériel spécifique.
Avant de prendre la route, nous vous invitons à prendre connaissance des conseils et règles de sécurité à respecter afin de profiter pleinement de votre Charrette :
Avant chaque utilisation, effectuez toujours une vérification des principaux organes de conduite et de sécurité : serrage de la barre d’attelage, de la potence, et des roues, tension des câbles de frein à inertie, pression des pneus et efficacité des freins.
Portez toujours un équipement approprié, en France. Le port d’un gilet rétro-réfléchissant certifié est obligatoire pour tout cycliste, et son éventuel passager, circulant hors agglomération, la nuit, ou lorsque la visibilité est insuffisante.
Respectez, en toutes circonstances le code de la route du pays dans lequel vous vous déplacez. Les législations concernant les équipements obligatoires et le code de la route sont différents d’un pays à l’autre.
Assurez-vous d’être visible et de pouvoir avertir les autres usagers de votre présence. En France il est obligatoire de disposer de catadioptres ou bande réfléchissantes sur les pneus, feux de position et d’un avertisseur sonore audible à 50m.
Le chargement de votre charrette doit être stable, ne pas être dangereux pour les usagers et enfants installés dans la charrette/les siège(s) enfant, le siège bébé  et ne pas dépasser de la caisse.
Le chargement de votre charrette sur le fond de caisse doit être solidement arrimé à l’aide de sangles et ne pas dépasser.

# Indications de chargement

Poids Total Autorisé en Charge : 300kg


# Outillage nécessaire

 * Jeu de clés allen
 * Clé à molette
 * Pince multiprise
 * clé a rayon
 * banc de dévoilage
 * maillet
 * planche de bois
 * démonte pneu
 * kit réparation

# Aspects mécaniques

## Réglage de la hauteur de selle

Le réglage adéquat est obtenu en positionnant la pédale en position basse, lorsque vous posez votre talon sur la pédale, votre jambe doit être tendue sans être raide. Le réglage s’effectue en desserrant la vis du collier. Une fois la hauteur ajustée, serrez la vis.

Veillez à ce que le porte-bagage arrière, s’il existe, ne gène pas l’attelage de la remorque.
Réglage des leviers de frein sur votre vélo
Afin que les leviers de frein tombent directement sous les doigts et diminuer le temps de réaction au freinage, la bonne position des leviers est dans le prolongement de vos avant-bras.

## Le freinage à inertie

### Rodage des patins

Il faut compter entre 20 et 40 arrêts complets pour roder les patins. Vous remarquerez peut-être une augmentation de la force de freinage dès votre première sortie. Les freins peuvent se montrer bruyants occasionnellement non seulement au cours de la période de rodage, mais également au-delà, pendant toute la durée de vie des patins. Le bruit dépend de facteurs tels que le réglage des freins, le poids du cycliste, sa manière de rouler et de freiner ainsi que les conditions dans lesquelles s'effectue la sortie (par exemple : poussière, sol, contamination des surfaces de frottement, etc.).

### Entretien et nettoyage

Le vélo et ses freins à disque doivent être entretenus avec grand soin. Dans des conditions normales d'utilisation, il n'est pas nécessaire de nettoyer l'étrier, le rotor ou les patins. Si le nettoyage s'avère nécessaire, n'utilisez que de l'eau et du liquide à vaisselle pour nettoyer l'étrier et le rotor, et assurez-vous de bien rincer le rotor de sorte qu'il ne reste aucune trace de savon. Séchez en essuyant avec du papier essuie-tout.

### Alignement des freins et usure des étriers de frein à disque

Bien vérifier l'alignement des freins sur les deux roues arrières. Le patin fixe doit être placé au plus près du disque. La partie mobile de l'étrier vient s'écraser sur le disque. Les freins sont à resserrer régulièrement selon l'utilisation.

![Réglage des étriers de frein](Equipement/manuel_montage/img/02.png)

### Réglage du frein à inertie

La mise en tension du frein à inertie se fait en 3 étapes :
 1. Vérifier l'alignement des étriers de frein par rapport au disque et les régler si besoin (cf  figure ci-dessus )
 2. Insérer le crochet dans le trou central sur la plaque de direction. cela immobilise le timon et contraint le ressort. Cela engendre un espacement du timon et de la plaque de direction.
 3. Dévisser les 2 vis en M6 sur les cotés. Prendre les câbles de freins en main et tirer dessus au maximum possible. Les étriers se serrent sur les disque de freinage. Resserrer les vis en M6. La remorque ne peut pas bouger, les 2 roues arrière sont bloqués.
 4. Enlever le crochet, le timon vient buter sur la plaque de direction, les freins se relâchent et la remorque peut rouler. Verifier que le frein à inertie fonctionne bien en donnant de bref acoups. C est bon vous pouvez rouler !


 ![Réglage du frein à inertie](Equipement/manuel_montage/img/03.jpg)

## Cadre d'utilisation

La charrette est prévue pour une utilisation sur route goudronnée, sur chemin en gravier ou sur tout chemin cyclable. Les trois roues doivent toujours rester en contact avec le sol. La charrette n’est en aucun cas prévu pour une utilisation de type «tout-terrain» ou pour un usage en compétition quelle qu’elle soit. La charrette est prévue pour être tractée par une personne adulte d’une taille minimum de 1,50 mètre et d’un poids minimum de 50 kg.
La charge totale admissible est de 300 kg maximum.
Elle ne doit en aucun cas être dépassée.

Pour votre sécurité, ne mettez jamais la béquille dans une pente ou sur un revêtement meuble. Lorsque vous retirez la béquille prenez garde à l’élan du vélo vers l’avant, gardez les doigts sur les leviers de frein afin d’arrêter immédiatement le vélo une fois la béquille relevée
Ne pas se lever de la selle tant que la remorque n’est pas à l’arrêt.
La prise en main d’une remorque à 3 roues est rapide mais nécessite de s’entraîner au début en faisant quelques tours dans un endroit dégagé afin de pouvoir en appréhender les dimensions en toute sécurité

### Freiner et arrêter la charrette

Une charrette est plus lourde et plus large (93cm) qu’une remorque classique, particulièrement lorsqu’elle est chargé, ce qui entraîne une inertie plus importante dans les manœuvres. Il est donc impératif de tenir compte des réactions plus lentes par rapport à un vélo classique afin de pouvoir anticiper, cela signifie qu’il faudra initier les freinages plus tôt et bien anticiper les trajectoires.
Votre charrette fait un mètre de large, il conviendra d’adapter votre position sur la chaussée pour ne pas heurter le trottoir ou tout autre obstacle.

## Insérer le jeu de direction

Le jeu de direction est composé des pièces suivantes et se monte a l’aide d’un maillet.
Nous utilisons un jeu 1’1 /8 semi-interne diametre cuvette 44mm à roulement industriel.

![Insérer le jeu de direction](Equipement/manuel_montage/img/04.png)

# Contrôle réguliers et changement des pièces d’usure

Vérifiez régulièrement les pièces d’usure de votre charrette de manière à ne prendre aucun risque et à ne pas vous retrouver bloqué en cours de déplacement. Si vous avez un doute quant à l’usure de certains composants, veuillez consulter le fabriquant ou une personne qualifiée.

Voici la liste des pièces d’usure  et points de contrôle qui doivent être vérifiées régulièrement et nécessitent une maintenance à différentes échéances.

## Pièces d’usure

### Freinage

 * Plaquettes
 * disques
 * étriers
 * coude 90° de freinage
 * gaines et câbles

### Roues

 * Pneus,
 * chambre à air

### Transmission

 * Roulement linéaire du frein à inertie
 * jeu de direction interne 1’1/8

# Fabrication de roue

**Un grand merci à Sheldon Brown et Michel Gagnon**

[Documentation de rayonnage en français sur internet](http://mgagnon.net/velo/roue_montage.fr.php)

[L'original en anglais](https://www.sheldonbrown.com/wheelbuild.html)


Un monteur de roues expérimenté peut aisément monter une roue en moins d’une heure. Néanmoins, le débutant doit s’attendre à y consacrer plusieurs heures. Il est préférable de répartir ce temps en plusieurs séances pour éviter de perdre patience et de massacrer une bonne roue en cours de route.
Cet article présente le montage d’une roue arrière, car c’est la plus complexe. Pour monter une roue avant, il suffit d’éviter tout ce qui ne s’y applique pas. Les illustrations sont celles d’une roue à 36 rayons croisés trois fois. Le principe de montage d’une roue de 32 rayons (ou de tout autre multiple de 4 rayons) est le même, comme nous le verrons plus loin.
Vous aurez besoin d’un petit tournevis à lame plate et d’une clé à rayons. (J’utilise une clé à rayons DT, mais la plupart des gens ne sont pas prêts à investir 75 $ [50 €] pour une telle clé à rayons.) Parmi les outils abordables, ma clé favorite est une clé à rayons en plastique avec embout en métal (appelée « Spokey »). Vous aurez également besoin d’un petit tournevis plat et il est préférable d’avoir aussi un banc d’alignement et une jauge de centrage.
Le banc d’alignement et la jauge de centrage sont de loin les équipements les plus dispendieux. On peut toutefois s’en passer en utilisant des outils improvisés ou le vélo lui-même. Si votre budget est serré, lisez les sections de cet article sur l’ajustement préliminaire et la mise en tension pour connaître la méthode et les termes, puis la section sur les équipements improvisés à  la fin de l’article.
On peut également utiliser un tensiomètre à rayons et un tournevis électrique muni de l’embout approprié. Mon embout préféré est un tournevis Phillips usé duquel j’ai limé deux des quatre pans, afin d’obtenir un tournevis à lame plate et pointue. La pointe reste donc centrée sur l’écrou et ne peut glisser.
La longueur des rayons est déterminée en mesurant de l’intérieur de l’épaule jusqu’à la fin de l’extrémité filetée. Elle s’exprime en millimètres.
La longueur des rayons dépend du type de moyeu et de jante ainsi que du type de laçage. Lorsque vous achetez des rayons, le vendeur devrait pouvoir déterminer la ou les longueurs requises. La plupart des ateliers utilisent aujourd’hui un programme appelé Spokemaster qui est inclus dans une base de données appelée Bike-alog on Disk.
Il existe plusieurs calculateurs de rayons disponibles par internet si vous voulez calculer vous-mêmes leur longueur. En voici quelques-uns :

 * le chiffrier Excel de Damon Rinard : comporte une base de données des jantes et des moyeux ;
 * le calculateur de Danny Epstein : il faut saisir les dimensions du moyeu et de la jante ;
 * le calculateur de Roger Musson (sur le site de Wheelpro): comporte une base de données de jantes et de moyeux ;
 * le calculateur Javascript de Jocelyn Ouellet (en français) sur le site de DocVélo : il faut saisir les dimensions du moyeu et de la jante ;
 * le calculateur graphique Java de Pete Gray (en anglais), basé sur les formules de Jobst Brandt (auteur de The Bicycle Wheel) sur le site de DocVélo ;
 * Le livre Sutherland’s Handbook for Bicycle Mechanics (6e édition) comporte des tableaux et des graphiques pour déterminer les longueurs de rayons.

Vous pouvez aussi mesurer les rayons d’une roue lacée avec un moyeu et une jante semblables et selon le même patron. Les mesures seront assez proches de la longueur nécessaire. Les calculateurs de rayons donnent des valeurs au dixième de millimètre ; or, les rayons sont généralement vendus au millimètre près et certaines marques sont vendues uniquement en longueurs multiples de 2 mm. En général, il est préférable d’arrondir vers le haut la valeur calculée.
l faut d’abord lubrifier les filets des rayons et les trous et les œillets de la jante avec de la graisse légère ou de l’huile afin de permettre aux écrous de tourner librement et de visser très fermement les rayons. Cela s’avère moins important de nos jours, étant donné la bonne qualité des rayons, des écrous et des jantes modernes. Néanmoins, il s’agit là d’une pratique recommandée pour permettre de tendre suffisamment les rayons pour que la roue ne voile pas.
Dans le cas d’une roue arrière à dérailleur, on recommande de lubrifier seulement les rayons et les œillets de droite, car les rayons de gauche sont peu tendus. Il est donc facile de serrer convenablement les écrous de ces rayons à sec et ceux-ci risquent moins de se desserrer d’eux-mêmes. En fait, avec les jantes dont les trous sont enfoncés, on suggère même d’appliquer un produit grippant comme le Spoke Prep de Wheelsmith sur les filets des rayons arrière gauche afin d’éviter le desserrement intempestif (sur les autres jantes, ce n’est pas nécessaire car la pression d’air de la chambre à air maintient les écrous en place).
On peut lubrifier rapidement les filets des rayons en en prenant une poignée, en la tapotant sur le banc de travail pour aligner les rayons, puis en les trempant dans un motton de graisse déposé dans une assiette de plastique. Et on peut lubrifier les trous dans la jante en appliquant de la graisse avec un rayon.
Si vous transférez les rayons sur une nouvelle jante, vous pouvez lubrifier les filets sans enlever les rayons du moyeu en suivant les instructions de cet article de Jobst Brandt sur la réutilisation des rayons. Suivez ensuite le mode d’emploi de cette page en sautant la section sur le laçage.
Attention:

 * Si vous récupérez les composantes d’une roue, enlevez la roue libre ou la cassette ainsi que le frein à tambour ou à disque du moyeu avant d’enlever les rayons, de les couper ou de les transférer sur une autre roue, car il sera impossible de le faire sur un moyeu seul. Cela vous permettra de bien inspecter les composantes, de remplacer les rayons endommagés (même en cas de transfert).
 * Même lors du transfert de rayons sur une nouvelle jante, assurez-vous d’avoir quelques rayons de rechange au cas où certains d’entre eux (ou certains écrous) seraient endommagés.
 * Des pinces bloquantes peuvent être utiles pour défaire des écrous arrondis.

N.D.T. On peut souvent faire des miracles avec les écrous arrondis ou figés en réduisant drastiquement la tension de tous les rayons.

## Le laçage

![Représentation des quatre nappes de rayons](Equipement/manuel_montage/img/05.gif)

Toutes les illustrations de cet article montrent une roue telle qu’on la voit de droite (côté cassette ou roue libre).
Voici le code de couleur utilisé pour distinguer les rayons :

 * les rayons de queue de droite ;
 * les rayons de queue de gauche ;
 * les rayons de tête de droite ;
 * les rayons de tête de gauche.
Pour lacer la roue, nous vous recommandons de vous asseoir, et de placer la jante sur vos genoux en l’appuyant contre la taille. Les monteurs professionnels placent tous les rayons en même temps dans le moyeu puis insèrent l’extrémité filetée dans la jante. Cette technique, qui permet de travailler plus rapidement, est source d’erreur pour le monteur occasionnel.
Nous conseillons plutôt d’insérer et de monter un « groupe » de rayons à la fois. La moitié des rayons partent du flasque de droite tandis que l’autre moitié part de celui de gauche. Et de chaque côté du moyeu, la moitié sont des rayons de queue tandis que l’autre moitié sont des rayons de tête.
Pour faciliter l’identification des rayons, nous avons identifié chacun de ces groupes par une couleur différente sur les illustrations et dans le texte, tel qu’indiqué ci-contre.

### Vos roues n’ont pas 36 rayons ?

Les instructions qui suivent sont écrites pour une roue de 36 rayons croisés trois fois, mais elles s’adaptent facilement pour des roues traditionnelles qui ont un autre multiple de 4 rayons. Par exemple, pour monter une roue de 32 rayons :

 * lire « 32 » aux endroits où il est écrit « 36 » ;
 * lire « 16 » aux endroits où il est écrit « 18 » ;
 * lire « 8 » aux endroits où il est écrit « 9 » ;
 * lire « 7 » aux endroits où il est écrit « 8 » ;
Si le nombre de croisements n’est pas de trois, la technique demeure la même. Seul le croisement le plus éloigné du moyeu est entrelacé.

### Devrait-on placer les rayons de queue à l’intérieur ou à l’extérieur du flasque ?

En théorie, les roues arrière à cassette devraient être lacées en plaçant les rayons de queue à l’intérieur du flasque (la tête à l’extérieur). Voici pourquoi :

 1. Les rayons plient légèrement au point de croisement le plus éloigné du moyeu. Lorsqu’on fournit un grand effort, surtout avec un petit développement, les rayons de queue se raidissent sous l’effort tandis que les rayons de tête courbent davantage. Lorsqu’on fournit un effort avec une roue lacée avec les rayons de queue à l’extérieur, le point de croisement est tiré vers l’extérieur et peut même parfois frotter contre le dérailleur.
 2. Si la chaîne est déportée à l’intérieur parce que le dérailleur est mal ajusté ou plié, le dérailleur et la chaîne risquent de bloquer plus sérieusement entre les rayons et la cassette si ceux-ci sont orientés de façon à ce que la chaîne se coince vers l’intérieur sous l’effort.
 Note. Par contre, pour un vélo à pignon fixe ou avec freinage à rétropédalage, il est préférable de lacer les rayons de queue à l’extérieur, car c’est au freinage que les efforts sont les plus grands et que la chaîne est la plus susceptible de se coincer.
 3. Lorsque la chaîne est déportée à l’intérieur, elle risque d’endommager et d’affaiblir les rayons sur lesquels elle frotte. Il est préférable de protéger les rayons de queue (les plus sollicités) contre ce type de dommage en les plaçant à l’intérieur.

Cela ne change pas grand chose si, du côté gauche, vous lacez les rayons de queue à l’intérieur ou à l’extérieur du flasque. La roue est toutefois plus facile à lacer si les rayons de queue des deux côtés sont tous à l’intérieur.

### Le premier groupe : les rayons de queue de droite

On s’attardera d’abord au positionnement du rayon maître, vu son importance. On traitera ensuite du laçage des autres rayons.

#### Un rayon de queue important : le rayon maître
![L’installation du rayon maître](Equipement/manuel_montage/img/06.gif)

Le rayon maître est le premier qu’on installe. Il doit être bien positionné en tenant compte de la position du trou de valve ainsi que de l’orientation des orifices des rayons. Le rayon maître est un rayon de queue côté cassette. Comme il s’agit d’un rayon de queue, il devrait être à l’intérieur du flasque (voir l’encadré ci-haut).
On l’enfile donc à partir de l’extérieur du flasque, de sorte que sa tête se retrouve à l’extérieur.

La coutume veut que l’on oriente la jante pour que l’étiquette soit lisible du côté droit du vélo. De même, si le corps du moyeu porte une identification visible, on la place pour qu’elle puisse être lue en regardant par le trou de valve. Il s’agit de traditions purement esthétiques qui n’affectent en rien le rendement de la roue, mais qui permettent de reconnaître un bon artisan.
Les jantes sont percées à droite ou à gauche. On parle ici de la position du trou de valve par rapport à ceux des rayons. Les trous des rayons ne sont pas au centre de la jante mais sont décalés en alternance de chaque côté de la ligne de centre. Les orifices de droite servent aux rayons qui vont au flasque de droite, tandis que ceux de gauche servent à ceux qui vont au flasque de gauche. Pour les fins de cet article, on dira qu’une jante est percée à droite si le trou de rayon immédiatement devant le trou de valve est orienté vers la droite (comme celle illustrée dans cet article).

 * Si la jante est percée à droite, on place le rayon maître dans le trou situé immédiatement à l’avant du trou de valve (c’est-à-dire à sa droite lorsqu’on tient la roue comme sur l’illustration).
 * Si la jante est percée à gauche, on place le rayon maître dans le 2e trou situé à l’avant du trou de valve. En plaçant ainsi le rayon maître, les quatre rayons qui entourent le trou de valve seront orientés de façon à s’éloigner de la valve et à en assurer le meilleur accès possible.

Vissez ensuite un écrou pour tenir le rayon maître en place. Quelques tours d’écrou suffisent pour l’instant.

\clearpage

#### Les autres rayons de queue de droite

![Les rayons de queue de droite](Equipement/manuel_montage/img/07.gif)

Lorsqu’on regarde la roue du côté droit (côté cassette), le rayon maître fait tourner le moyeu dans le sens anti-horaire.

Placez ensuite les 8 autres rayons de queue de droite. On place un rayon à tous les deux trous dans le flasque et à tous les quatre trous dans la jante. Vissez un écrou sur chacun des rayons.

La roue comporte maintenant 9 rayons de queue (comme ci-contre). Vérifiez alors la roue pour vous assurer que les rayons sont bien placés et que l’espacement est régulier. Il doit y avoir :

 * une tête de rayon, un trou, une tête de rayon, un trou... sur le flasque de droite ;
 * un écrou (dans un trou de droite), trois trous de rayons vides, un écrou, trois trous de rayons vides... sur la jante.

Corrigez au besoin.

\clearpage

### Le deuxième groupe : les rayons de queue de gauche

![La roue, avec le 10e rayon](Equipement/manuel_montage/img/08.gif)

Tournez la roue du côté gauche (pour que la cassette soit éloignée de vous). En examinant le moyeu, vous constaterez que les orifices du flasque de gauche sont alignés entre ceux du flasque de droite. Vous pouvez aussi insérer un rayon dans le flasque de gauche ; vous constaterez qu’il bute sur le flasque de droite.

En tenant toujours la roue du côté gauche, placez-la pour que le trou de valve soit en haut. Le rayon maître est alors situé à gauche du trou de valve.

 * Si le rayon maître est immédiatement à gauche du trou de valve, insérez un rayon dans le flasque de gauche tout juste à gauche de la tête du rayon maître.
Placez-le ensuite pour que l’extrémité filetée se trouve dans le trou de la jante situé immédiatement à gauche de celui du rayon maître.
 * Si le rayon maître est séparé du trou de valve par un trou vide (jante percée à gauche), insérez un rayon dans le flasque de gauche tout juste à droite de la tête du rayon maître.
Placez-le ensuite pour que l’extrémité filetée se trouve dans le trou de la jante situé entre le trou de valve et celui du rayon maître.

Vissez ensuite un écrou pour tenir ce rayon en place.

S’il est bien placé, ce rayon ne croise pas le rayon maître, mais il est plutôt à gauche ou à droite de celui-ci. Et comme il s’agit d’un rayon de queue, il se trouve à l’intérieur du flasque tandis que sa tête est à l’extérieur.

![La roue, avec 18 rayons](Equipement/manuel_montage/img/09.gif)

Placez ensuite les 8 autres rayons de queue de gauche. Insérez un rayon à tous les deux trous dans le flasque et à tous les quatre trous dans la jante. Vissez un écrou sur chacun des rayons.

La roue comporte maintenant 18 rayons, tous de queue. Vérifiez alors la roue pour vous assurer que les rayons sont bien placés et que l’espacement est régulier (comme ci-contre). Il doit y avoir :

 * une tête de rayon, un trou, une tête de rayon, un trou... sur le flasque de droite (ces rayons avaient été placés à l’étape précédente) ;
 * une tête de rayon, un trou, une tête de rayon, un trou... sur le flasque de gauche ;
 * deux écrous (un écrou dans un trou de droite et un écrou dans un trou de gauche), deux trous de rayons vides, deux écrous, deux trous de rayons vides... sur la jante.

Corrigez au besoin.

\clearpage

### Les rayons de tête de droite

![L’installation du premier rayon de tête de droite.](Equipement/manuel_montage/img/10.gif)

Tournez de nouveau la roue pour que le côté droit (côté cassette) soit vers vous.

Insérez maintenant un rayon dans un des trous vides. Mais cette fois-ci, insérez-le à partir de l’intérieur du flasque. Tout en tenant fermement la jante, tournez le moyeu le plus loin possible en direction horaire, afin de tendre les rayons de queue préalablement installés.

Lacez ce rayon de tête de façon à ce qu’il croise 3 rayons de queue de droite. Lors des deux premiers croisements, ce rayon de tête doit passer à l’extérieur des rayons de queue ; par contre, au troisième croisement, il faut le lacer pour qu’il passe à l’intérieur du rayon de queue. Il faut plier un peu ce rayon pour le lacer correctement.

Après avoir lacé ce rayon, il est techniquement possible d’insérer l’extrémité filetée dans deux trous de rayons. Insérez ce rayon dans le trou qui est du bon côté de la jante (le droit). Ce rayon ne doit pas se trouver à côté d’un rayon de queue de droite.

Vissez ensuite un écrou pour tenir ce rayon en place.

Lacez de la même façon les huit autres rayons de tête de droite et vissez un écrou sur chacun d’eux.
Vérifiez la roue et corrigez au besoin.

\clearpage

### Les rayons de tête de gauche

![La roue, avec tous ses rayons](Equipement/manuel_montage/img/11.gif)

La roue munie de tous ses rayons. Remarquez que les rayons sont placés de façon à dégager le trou de valve.
Il ne reste plus qu’à tendre les rayons et à redresser la roue.

Tournez de nouveau la roue pour que le côté gauche soit vers vous.

Insérez maintenant un rayon dans un des trous vides, à partir de l’intérieur du flasque.

Lacez le rayon de façon à ce qu’il croise 3 rayons de queue de gauche. Lors des deux premiers croisements, ce rayon de tête doit passer à l’extérieur des rayons de queue ; par contre, au troisième croisement, il faut le lacer pour qu’il passe à l’intérieur du rayon de queue. Il faut plier un peu ce rayon pour le lacer correctement.

Insérez le rayon dans le trou approprié de la jante. Ce rayon ne doit pas se trouver à côté d’un rayon de queue de gauche.

Vissez ensuite un écrou pour tenir ce rayon en place.

Placez les huit autres rayons de tête de gauche et vissez un écrou sur chacun d’eux.

Vérifiez le laçage, notamment en suivant les trous de rayons sur la jante pour vous assurer que les rayons alternent entre la gauche et la droite. La roue doit ressembler à celle de la figure ci-contre. Corrigez au besoin.


## L’alignement et la mise en tension de la roue

Placez maintenant la roue dans un banc d’alignement.

Avec un peu de chance, la roue est déjà relativement droite, mais ne soyez pas surpris si elle ne l’est pas. Si les rayons sont encore lâches à un point tel que vous pouvez facilement bouger la jante, vissez chacun des écrous d’un tour supplémentaire. On recommande de le faire en partant du trou de valve pour être certain de visser tous les rayons.

Assurez-vous de visser les écrous dans le bon sens !  Le sens de vissage est évident lorsqu’on les visse avec le tournevis puisqu’on se place alors face à la jante, mais lorsqu’on les visse avec la clé à rayons, il ne faut pas oublier qu’on voit les écrous à l’envers... et qu’il faut donc penser à l’envers. Bref, rappelez-vous qu’on visse en tournant dans le sens des aiguilles d’une montre, tel que vu lorsqu’on est face à la jante !

Continuez de visser les écrous, un tour à la fois, jusqu’à ce que les rayons commencent à se tendre et que la roue gagne en rigidité. Vous pouvez alors graduellement lui donner sa forme. Quatre actions sont nécessaires :

 * le dévoilage ;
 * la correction des sauts ;
 * le centrage ;
 * la mise en tension.

Il faut vérifier ces quatre facteurs en cours de route et agir sur celui qui est le plus problématique. Tout au long de ce processus, il est possible de voir ou de ressentir les irrégularités. Lorsque la roue est presque complètement alignée, il est possible d’entendre le frottement des guides du banc d’alignement sur la jante. Certains bancs de haut de gamme ont des jauges indicatrices, mais ce n’est pas nécessaire. Pour éviter de tourner en rond, essayez de faire chacun des ajustements en affectant le moins possible les autres. Voici comment.

### Le dévoilage

Correction du déport latéral de la jante

Faites tourner la roue dans le support et trouvez le point qui s’écarte le plus de la trajectoire de la plus grande partie de la jante. Si, par exemple, cette portion est trop à gauche, resserrez les écrous des rayons de droite et desserrez ceux des rayons de gauche, de façon à tasser la jante vers la droite. Si vous resserrez les écrous de droite et desserrez ceux de gauche de façon identique, vous pouvez déplacer la jante vers la droite sans (trop) affecter la rondeur de la roue.

Si par exemple, la jante courbe vers la gauche et que le centre de ce voile est entre deux rayons, vous pouvez resserrer le rayon de droite de 1/4 tour et celui de gauche de 1/4 tour. De même, si le centre du voile est vis-à-vis un rayon de droite, vous pouvez resserrer ce rayon de 1/4 tour et desserrer les deux rayons de gauche adjacents de 1/8 tour.

Corrigez le pire voilement à gauche, puis le pire voilement à droite... sans essayer de corriger à la perfection chacun des problèmes. Vous dévoilerez ainsi graduellement la roue.

### La correction des sauts

Correction du déport de haut en bas de la jante

Trouvez l’endroit le plus haut sur la jante et travaillez de la même façon. Si le centre de ce point haut est situé entre deux rayons, resserrez chacun d’eux de 1/2 tour. S’il est situé vis-à-vis un rayon, resserrez celui-ci de 1 tour et les deux rayons avoisinants de 1/2 tour. Il faut visser plus longtemps pour corriger un saut qu’un voile.

Généralement, la correction des sauts s’effectue uniquement en tendant les rayons, ce qui permet d’augmenter graduellement la tension de la roue.

### Le centrage

Lorsque la roue est dévoilée à quelques millimètres près, il est temps de vérifier son centrage. Placez le plongeur ajustable de la jauge de centrage sur l’essieu et ajustez-le de façon à ce que les deux extrémités de la jauge s’appuient sur la jante et que le plongeur s’appuie sur l’écrou extérieur de l’essieu. Tournez ensuite la jauge et faites la même vérification sans ajuster le plongeur.
Si la jauge oscille autour du plongeur, les rayons de ce côté doivent être tendus pour recentrer la roue.
Si au contraire le plongeur ne touche pas à l’écrou, ce sont les rayons de l’autre côté qui doivent être tendus.

Si la roue est décentrée de plus de 2 ou 3 mm, vissez tous les rayons du côté où il faut tirer la jante de 1/2 tour (ou plus, au besoin).

Si par contre, le centrage est précis à 1 ou 2 mm près, reprenez l’ajustement latéral, mais en « tirant » la jante du bon côté. Ainsi, s’il faut déplacer la jante vers la droite pour mieux la centrer, trouvez l’endroit où elle est la plus voilée vers la gauche, corrigez-le, puis encore l’endroit où elle est la plus voilée vers la gauche... Vérifiez également l’alignement vertical ; si les sauts sont pires que les voiles, corrigez alors l’alignement vertical.

La surface d’une jante machinée peut être irrégulière à l’endroit où elle est assemblée, soit généralement à l’opposé du trou de valve. Si la jante est soudée, un meulage excessif du cordon de soudure peut avoir laissé une légère dépression. Et si elle est assemblée avec un tenon, les deux extrémités peuvent ne pas s’aligner parfaitement. Vous devrez peut-être tolérer un certain saut à cet endroit, ou l’aligner à l’œil sur le dessous de la jante.

### La mise en tension

Vous devez tenir compte de la tension du côté cassette de la roue. Vous pouvez la vérifier de trois manières :

 * Selon la difficulté à tourner la clé à rayons. Vous approchez de la tension maximale lorsque vous avez peur d’arrondir les écrous au lieu de les serrer. Jusqu’aux années 1980, c’était là la tension maximale possible pour éviter de briser les filets des écrous. De nos jours, les filets des rayons et des écrous sont de machinés avec une meilleure précision de sorte qu’il est maintenant possible de trop tendre les rayons et de causer un bris de la jante.
 * Selon la note produite par les rayons. On peut pincer les rayons comme des cordes de guitare et évaluer la tension à l’oreille. Même sans piano, on peut comparer la note produite avec celle d’une bonne roue et ainsi avoir une idée de la tension. D’ailleurs, avant d’utiliser un tensiomètre, j’utilisais une cassette sur laquelle j’avais enregistré le fa dièse au piano, ce qui correspond approximativement à la note produite par un rayon en acier inoxydable de longueur moyenne. (Pour plus de détails sur cette méthode, voir l’article de John Allen : Check Spoke Tension by Ear.)
 * Avec un tensiomètre. C’est évidemment la meilleure méthode. Tout atelier bien équipé devrait donc en avoir un. La tension du côté droit de la roue doit être celle prévue par les fabricants pour les rayons et la jante utilisée. Mais surtout, la tension devrait être égale. On n’a pas à s’inquiéter de la tension des rayons de gauche d’une roue arrière, en autant que celle-ci soit semblable pour tous les rayons. Car si la roue est proprement centrée, ces rayons seront plus lâches que ceux de droite. La tension d’une roue symétrique (comme une roue avant sans frein à disque) devrait être à peu près la même pour tous les rayons.

# Points de contrôle

## Points de contrôle avant départ

 Avant chaque utilisation de la charrette:

 * Contrôlez les freins de votre vélo.
 * Contrôlez la pression et l’état des pneus de votre vélo et de la charrette.
 * Contrôlez le serrage des roues.
 * Contrôlez le serrage de la potence et des pièces de la direction.
 * Contrôlez le jeu du système de frein à inertie.
 * Contrôlez la vis de maintien de la platine au bout du timon.
 * Contrôlez le verrouillage du support d’attache sur la tige de selle
 * Contrôlez l’attache de votre tige de selle
 * Contrôlez l’état de charge de votre batterie.

## Points de contrôle mensuels

 * Contrôlez l’usure des plaquettes de frein.
 * Contrôlez le voile des roues et la tension des rayons.
 * Contrôlez le jeu dans les différents éléments de la direction et réglez ce jeu le cas échéant.
 * Contrôlez l’ensemble des pièces de la transmission  de votre vélo qui sont soumises à des efforts particulièrement importants et lubrifiez les si nécessaire.

## Points de contrôle semestriels

 * Effectuez un nettoyage complet de la charrette.
 * Contrôlez le cadre (pas de fissure ou de déformation).
 * Contrôlez tous les serrages des vis de fixation et d’assemblage.
 * Contrôlez l’état du ou des câble(s) de la transmission.
 * Contrôlez l’état de l’extrémité des câbles de direction et nettoyez-les avec un produit dégraissant si nécessaire. Si vous ressentez une résistance anormale dans la direction de votre charrette veuillez consulter.
 * Faîtes réaliser un entretien du vélo et un diagnostic (mise à jour) de la motorisation de la charrette par un membre de Cyclonomia.



![Charrettes prêtes à l'action](Equipement/manuel_montage/img/11.jpg)
